from django.apps import AppConfig


class CmsPutPostConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "cms_put_post"
