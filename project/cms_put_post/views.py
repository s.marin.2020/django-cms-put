from django.shortcuts import render, redirect, get_object_or_404
from .models import Contenido
from django.views.decorators.csrf import csrf_exempt



@csrf_exempt
def get_content(request, llave):
    if request.method == "PUT":
        valor = request.body.decode('utf-8')
        c = Contenido.objects.update_or_create(
                clave=llave,
                defaults={'valor': valor}
            )
    contenido = get_object_or_404(Contenido, clave=llave)
    return render(request, 'cms_put_post/content.html', {'content': contenido})


def index(request):
    content_list = Contenido.objects.all()
    context = {'content': content_list}
    return render(request, 'cms_put_post/index.html', context)