from django.contrib import admin
from django.urls import include, path
from django.contrib.auth.views import LoginView as login

urlpatterns = [
    path("cms/", include('cms_put_post.urls')),
    path("admin/", admin.site.urls),
]
